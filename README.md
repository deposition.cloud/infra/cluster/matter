# Matter

> that which occupies space and possesses rest mass

Persistence layer

- Ceph via Rook

## Prerequisites

Requires an Internet facing MicroK8s cluster.

## Design

2 clusters on the same LAN.

The `live` cluster is the production cloud, a 3+ nodes high-availability bare metal setup.

The `omega` cluster is a few commits ahead and doubles as a fail-over 1-node VM cluster.

## Develop and Deploy

``` bash
pulumi config set kubernetes:context live # or
pulumi config set kubernetes:context omega 
```

Pre-requisites for Helm.

``` bash
helm repo add rook-release https://charts.rook.io/release
```

### Pulumi Stack Config

Ensure you have all the required configuration variables setup

``` bash
pulumi config set --plaintext domain deposition.cloud # replace with your domain
```

Enable the desired features in `Pulumi.<stackName>.yaml`

``` yaml
config:
  ...
  matter:features:
    cephOperator: true
    cephCluster: true
  ...
```

### Deploy

``` bash
pulumi up -f -y
```

Yey! :fireworks:

## Postresquisites



## Troubleshooting

Many things could go wrong. :smile:
