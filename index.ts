import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"

interface Features {
  cephOperator: boolean,
  cephCluster: boolean
}

let config = new pulumi.Config()

const stack = pulumi.getStack()
const tier = pulumi.getProject()

const features = config.requireObject<Features>('features')

const domain = config.require('domain')
const adminEmail = config.require('adminEmail')
console.log(`Booting up ${domain} persistence for ${adminEmail}...`)

const out: any = {}

/*
* ceph operator
*/

if (features.cephOperator) {
  const cephOperatorShortName = 'save'
  const cephOperatorPrefix = 'ceph-operator'
  const cephOperatorNamespace = new k8s.core.v1.Namespace(cephOperatorPrefix, {
    metadata: { labels: { name: cephOperatorPrefix, stack, tier } }
  })

  const cephOperator = new k8s.helm.v3.Chart(cephOperatorShortName, {
    repo: "rook-release",
    chart: "rook-ceph",
    namespace: cephOperatorNamespace.metadata.name,
    values: {}
  }, {
    parent: cephOperatorNamespace
  })

  if (features.cephCluster) {
    const cephClusterShortName = 'keep'
    const cephClusterPrefix = 'ceph-cluster'
    const cephClusterNamespace = new k8s.core.v1.Namespace(cephClusterPrefix, {
      metadata: { labels: { name: cephClusterPrefix, stack, tier } }
    }, {
      parent: cephOperator
    })

    const cephCluster = new k8s.helm.v3.Chart(cephClusterShortName, {
      repo: "rook-release",
      chart: "rook-ceph-cluster",
      namespace: cephClusterNamespace.metadata.name,
      values: {
        operatorNamespace: cephOperatorNamespace.metadata.name
      }
    }, {
      parent: cephClusterNamespace
    })
  }
}

export default {
  features,
  out
}